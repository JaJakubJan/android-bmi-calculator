package com.example.bmicalculator

data class BMICalculation(
        val value: BMI,
        val height: Int,
        val mass: Int,
        val system: MeasurementSystem
)