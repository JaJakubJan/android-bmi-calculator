package com.example.bmicalculator

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.example.bmicalculator.databinding.ActivityMainBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val HEIGHT = "height"
        const val WEIGHT = "weight"
        const val BMI = "bmi"
        const val BMI_COLOR = "bmiColor"
        const val SYSTEM = "system"
        const val CALCULATIONS = "bmiCalculations"
        val JSON_SERIALIZER: Gson = Gson()
    }
    private lateinit var binding: ActivityMainBinding
    private var system: MeasurementSystem = MeasurementSystem.METRIC
    private val calculator = BMICalculator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null)
            super.onRestoreInstanceState(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val languages = resources.getStringArray(R.array.system_array)
        val spinner = findViewById<Spinner>(R.id.systemList)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = createAdapterView(languages)
    }

    private fun createAdapterView(languages: Array<String>): AdapterView.OnItemSelectedListener {
        return object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>) {}
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                binding.apply {
                    if (languages[position] == "Metrics") {
                        system = MeasurementSystem.METRIC
                        heightTextView.text = getString(R.string.height_cm)
                        massTextView.text = getText(R.string.mass_kg)
                    } else {
                        system = MeasurementSystem.IMPERIAL
                        heightTextView.text = getString(R.string.height_in)
                        massTextView.text = getText(R.string.mass_lb)
                    }
                }
                if (view != null)
                    countButtonHandler(view)
            }
        }
    }

    fun countButtonHandler(view: View) {
        binding.apply {
            if (heightEditText.text.isBlank())
                heightEditText.error = getString(R.string.height_is_empty)
            val height: Int = heightEditText.text.toString().toInt()
            val mass: Int = massEditText.text.toString().toInt()
            val bmi: BMI = calculator.getBMIFrom(height, mass, system)
            val bmiInfo = (bmi.level.toString().toLowerCase(Locale.ROOT)).replace('_', ' ')
            val resBMI = "${bmi.value} $bmiInfo"
            bmiTextView.text = resBMI
            addCalculationToSharedPreferences(BMICalculation(bmi, height, mass, system))
            bmiTextView.setTextColor(bmi.level.color)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.apply {
            outState.putString(HEIGHT, heightEditText.text.toString());
            outState.putString(WEIGHT, massEditText.text.toString());
            outState.putString(BMI, bmiTextView.text.toString());
            outState.putInt(BMI_COLOR, bmiTextView.currentTextColor);
            outState.putString(SYSTEM, system.name);
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding.apply {
            heightEditText.setText(savedInstanceState.get(HEIGHT).toString())
            massEditText.setText(savedInstanceState.get(WEIGHT).toString())
            bmiTextView.text = savedInstanceState.get(BMI).toString()
            bmiTextView.setTextColor(savedInstanceState.getInt(BMI_COLOR))
            if(savedInstanceState.get(SYSTEM).toString() == MeasurementSystem.METRIC.name)
                system = MeasurementSystem.METRIC
            else
                system = MeasurementSystem.IMPERIAL
        }
    }
    private fun addCalculationToSharedPreferences(BMICalculation: BMICalculation) {
        val sharedPreferences: SharedPreferences = getSharedPreferences(BMI, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        val calculationType: Type = object : TypeToken<MutableList<BMICalculation>>() {}.type
        val rawCalculations: String? = sharedPreferences.getString(CALCULATIONS, "[]");
        val calculations = JSON_SERIALIZER.fromJson<MutableList<BMICalculation>>(rawCalculations, calculationType);
        if (calculations.size >= 10)
            calculations.removeAt(0)
        calculations.add(BMICalculation)
        val data: String = JSON_SERIALIZER.toJson(calculations)
        editor.putString(CALCULATIONS, data).apply()
        Log.i("BMIStoreValues", "Your BMI calculations: $data")
    }

    fun changeActivity(view: View) {
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }
}