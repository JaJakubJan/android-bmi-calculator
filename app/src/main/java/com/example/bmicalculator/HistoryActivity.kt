package com.example.bmicalculator

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.reflect.TypeToken

class HistoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        val spCalculations = getSharedPreferences(MainActivity.BMI, Context.MODE_PRIVATE)
                .getString(MainActivity.CALCULATIONS, "[]")
        val calculations = MainActivity.JSON_SERIALIZER.fromJson<List<BMICalculation>>(
                spCalculations, object : TypeToken<List<BMICalculation>>() {}.type)
        val rvCalculations = findViewById<View>(R.id.HistoryRecyclerView) as RecyclerView
        rvCalculations.layoutManager = LinearLayoutManager(this)
        rvCalculations.adapter = HistoryAdapter(calculations)
    }

    fun finishActivity(view: View) {
        finish()
    }
}