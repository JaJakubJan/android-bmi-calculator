package com.example.bmicalculator

import kotlin.math.pow
import kotlin.math.round

class BMICalculator{
    fun getBMIFrom(height: Int, weight: Int, system: MeasurementSystem): BMI {
        return if (height > 0 && weight > 0) getBMILvlFrom(calculate(height, weight, system))
        else BMI(0.0, BMILevel.UNKNOWN)
    }

    private fun calculate(height: Int, weight: Int, system: MeasurementSystem): Double {
        val heightUnit: HeightUnit = system.heightUnit;
        val weightUnit: WeightUnit = system.weightUnit;
        val res: Double = weight * weightUnit.factor / (height * heightUnit.factor).pow(2.0)
        return round(res * 100.0) / 100.0
    }

    fun getBMILvlFrom(value: Double): BMI {
        return BMI(
            value, BMILevel.values().first { lvl -> value >= lvl.minValue && value <= lvl.maxValue})
    }
}
