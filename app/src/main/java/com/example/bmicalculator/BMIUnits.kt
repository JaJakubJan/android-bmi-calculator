package com.example.bmicalculator

import android.graphics.Color

enum class BMILevel(val minValue: Double, val maxValue: Double, val color: Int) {
    UNKNOWN(Double.MIN_VALUE, 1.0, Color.GRAY),
    UNDERWEIGHT(1.0, 18.49, Color.BLUE),
    NORMAL(18.49, 24.9, Color.GREEN),
    OVERWEIGHT(24.9, 29.9, Color.rgb(255, 165, 0)),
    OBESE(29.9, 34.9, Color.MAGENTA),
    EXTREMELY_OBESE(34.9, Double.MAX_VALUE, Color.RED);
}

enum class MeasurementSystem(val heightUnit: HeightUnit, val weightUnit: WeightUnit) {
    METRIC(HeightUnit.CENTIMETER, WeightUnit.KILOGRAM),
    IMPERIAL(HeightUnit.INCH, WeightUnit.POUND)
}

enum class HeightUnit(val factor: Double, val shortcut: String) {
    CENTIMETER(0.01, "cm"),
    INCH(0.0254, "in")
}

enum class WeightUnit(val factor: Double, val shortcut: String) {
    KILOGRAM(1.0, "kg"),
    POUND(0.453592, "lb")
}





