package com.example.bmicalculator

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.history_activity_single_row.view.*

class HistoryAdapter(private val singleBmiCalculations: List<BMICalculation>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val bmi = view.bmi_elem;
        private val height = view.height_elem;
        private val mass = view.mass_elem;

        fun bind(singleBmiCalculation: BMICalculation) {
            val system: MeasurementSystem = singleBmiCalculation.system;
            val heightText = "${singleBmiCalculation.height} ${system.heightUnit.shortcut}"
            val massText = "${singleBmiCalculation.mass} ${system.weightUnit.shortcut}"
            val bmiText = singleBmiCalculation.value.value.toString()
            bmi.text = bmiText
            bmi.setTextColor(singleBmiCalculation.value.level.color)
            height.text = heightText
            mass.text = massText
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.history_activity_single_row,
                parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = singleBmiCalculations.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {is ViewHolder -> { holder.bind(singleBmiCalculations[position]) } }
    }
}
