package com.example.bmicalculator

data class BMI(val value: Double, val level: BMILevel)