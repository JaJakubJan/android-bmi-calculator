package com.example.bmicalculator

import org.junit.Test
import org.junit.Assert.*

class BMICalculatorTest {
    private val calculator = BMICalculator()

    @Test
    fun calculateValidNormalBMI() {
        val bmi: BMI = calculator.getBMIFrom(182, 82, MeasurementSystem.METRIC)
        assertEquals(24.76, bmi.value, 0.001)
        assertEquals(BMILevel.NORMAL, bmi.level)
    }

    @Test
    fun calculateValidLowBMI() {
        val bmi: BMI = calculator.getBMIFrom(182, 52, MeasurementSystem.METRIC)
        assertEquals(15.7, bmi.value, 0.001)
        assertEquals(BMILevel.UNDERWEIGHT, bmi.level)
    }
    @Test
    fun calculateValidHighBMI() {
        val bmi: BMI = calculator.getBMIFrom(180, 84, MeasurementSystem.METRIC)
        assertEquals(25.93, bmi.value, 0.001)
        assertEquals(BMILevel.OVERWEIGHT, bmi.level)
    }

    @Test
    fun calculateValidBMIForImperialSystem() {
        val bmi: BMI = calculator.getBMIFrom(71, 260, MeasurementSystem.IMPERIAL)
        assertEquals(36.26, bmi.value, 0.001)
        assertEquals(BMILevel.EXTREMELY_OBESE, bmi.level)
    }

    @Test
    fun calculateUnknownBMIForMetricalSystem() {
        val bmi: BMI = calculator.getBMIFrom(0, 82, MeasurementSystem.METRIC)
        assertEquals(0.0, bmi.value, 0.001)
        assertEquals(BMILevel.UNKNOWN, bmi.level)
    }

    @Test
    fun calculateUnknownBMIForImperialSystem() {
        val bmi: BMI = calculator.getBMIFrom(182, 0, MeasurementSystem.IMPERIAL)
        assertEquals(0.0, bmi.value, 0.001)
        assertEquals(BMILevel.UNKNOWN, bmi.level)
    }
}